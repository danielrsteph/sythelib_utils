from types import SimpleNamespace
from typing import Union, Optional


def antigcp() -> bool:
    """
    Allow virtual mouse to work in some cases where it otherwise would not e.g. some Java applications.
    Caution this patches the target application in memory.
    
    :return: bool 
    """
    pass


def bitmap_to_backbuffer(
    bmp_id: int, operation: int, x: Optional[int] = 0, y: Optional[int] = 0
) -> bool:
    """
    Draw the bitmap identified by bmp_id to the backbuffer at x,y using draw operation=0 for copy, 1 for
    bitwise AND, 2 for bitwise OR, 3 for bitwise XOR.
    
    :param bmp_id: int
    :param operation: int
    :param x: Optional[int], default = 0
    :param y: Optional[int], default = 0
    :return: bool 
    """
    pass


def bitmap_to_string(bmp_id: int) -> str:
    """
    Output the bitmap identified by bmp_id as a string for use in load_bitmap_from_string.
    
    :param bmp_id: int
    :return: str 
    """
    pass


def calculate_minimap_alignment(
    map_center_x: int,
    map_center_y: int,
    radius: int,
    wall_color: int,
    color_tolerance: int,
    deformation_tolerance: int,
) -> SimpleNamespace:
    """
    In games or target applications with a minimap, specify the center of the minimap with map_center_x
    and map_center_y and then the radius (half the minimap width). Set wall_color to some straight-line
    object that appears on the map such as a wall or boundary. The alignment_score returned will give an
    estimate of how rotationally 'off center' the map is (which can be useful to figure out which way
    you are pointing in a game for example.)
    
    :param map_center_x: int
    :param map_center_y: int
    :param radius: int
    :param wall_color: int
    :param color_tolerance: int
    :param deformation_tolerance: int
    :return: SimpleNamespace
    """
    pass


def calculate_minimap_movement(
    map_center_x: int,
    map_center_y: int,
    radius: int,
    point_count: int,
    rotation_tolerance: int,
    color_tolerance: int,
    deformation_tolerance: int,
    max_distance: int,
) -> SimpleNamespace:
    """
    In games or target applications with a minimap, specify the center of the minimap with map_center_x
    and map_center_y and then the radius (half the minimap width). point_count should be a reasonably
    high number for example 360 which represents how many points will be 'watched' by sythelib around
    the map. max_distance is the number of pixels a point can have moved since the last call.
    rotation_tolerance is the number of degrees of rotation of the minimap to tolerate (this should be
    low 0-5). Function needs to be called in a loop and will return the computed movement off center
    since the last call.
    
    :param map_center_x: int
    :param map_center_y: int
    :param radius: int
    :param point_count: int
    :param rotation_tolerance: int
    :param color_tolerance: int
    :param deformation_tolerance: int
    :param max_distance: int
    :return: SimpleNamespace
    """
    pass


def click_mouse(x: int, y: int, button: int) -> None:
    """
    Move the mouse (virtual or real) to the specified coordinates inside the selected window and then
    click a button. button=1 is left, button=2 is right, button=3 is middle.
    
    :param x: int
    :param y: int
    :param button: int
    :return: None
    """
    pass


def click_mouse_random_area(
    left: int, top: int, right: int, bottom: int, button: int
) -> None:
    """
    Move the mouse (virtual or real) to a random point within the specified coordinates inside the
    selected window and then click a button. button=1 is left, button=2 is right, button=3 is middle..
    
    :param left: int
    :param top: int
    :param right: int
    :param bottom: int
    :param button: int
    :return: None
    """
    pass


def create_rotated_bitmap(bmp_id: int, rotation_float: float) -> int:
    """
    Given a bitmap identified by bmp_id rotate it by rotation_float degrees and then create a new bitmap
    from it. The new bitmap's bmp_id is returned. 0 is returned on failure.
    
    :param bmp_id: int
    :param rotation_float: float
    :return: int
    """
    pass


def destroy_bitmap(bmp_id: int) -> None:
    """
    destroy_bitmap(bmp_id) -- ADVANCED USE ONLY. Free the memory used up by the bitmap identified by
    bmp_id. Warning attemping to reuse this bmp_id after running this function will result in a crash of
    your application.
    
    :param bmp_id: int
    :return: None
    """
    pass


def distance(left: int, top: int, right: int, bottom: int) -> int:
    """
    Compute the pixel distance between two points (left, top) and (right, bottom).
    
    :param left: int
    :param top: int
    :param right: int
    :param bottom: int
    :return: int
    """
    pass


def double_click_mouse(x: int, y: int, button: int) -> None:
    """
    Move the mouse (virtual or real) to the specified coordinates inside the selected window and then
    double click a button. button=1 is left, button=2 is right, button=3 is middle.
    
    :param x: int
    :param y: int
    :param button: int
    :return: None
    """
    pass


def dump_backbuffer() -> None:
    """
    Used for debugging. Open a window containing the current backbuffer. Calling the function again will
    refresh the view of the backbuffer in the window. If the window has been closed calling again will
    reopen the window with the new view.
    
    :return: None
    """
    pass


def dump_backbuffer_to_bitmap_string() -> str:
    """
    Create a string to be used by load_bitmap_from_string from the complete current backbuffer.
    
    :return: str
    """
    pass


def dump_backbuffer_to_bitmap_string_rect(
    left: int, top: int, right: int, bottom: int
) -> str:
    """
    Create a string to be used by load_bitmap_from_string from a region of the current backbuffer.
    
    :param left: int
    :param top: int
    :param right: int
    :param bottom: int
    :return: str
    """
    pass


def dump_window_structure() -> None:
    """
    Used for debugging. Dumps the internal window structure of the current ancestor window for use with
    select_inner_window.
    
    :return: None
    """
    pass


def erase_backbuffer() -> None:
    """
    Set all pixels in the current backbuffer to black RBG(0,0,0).
    
    :return: None
    """
    pass


def erase_backbuffer_rect(left: int, top: int, right: int, bottom: int) -> None:
    """
    Set all pixels in the specified region of the current backbuffer to black RGB(0,0,0).
    
    :param left: int
    :param top: int
    :param right: int
    :param bottom: int
    :return: None
    """
    pass


def find_bitmap(
    bmp_id: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    color_tolerance: Optional[int] = 0,
    deformation_tolerance: Optional[int] = 0,
    percent_match: Optional[int] = 99,
    closest: Optional[bool] = False,
    x: Optional[int] = 0,
    y: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Find a bitmap identified by bmp_id in the current backbuffer (the current scrap()ed screen). If
    left, top, right, and/or bottom are specified then search only that region otherwise search the
    whole backbuffer. percent_match is an integer 0-100 that represents how much of the bitmap must
    match for it to be consider a match. color_tolerance is how similar a color must be to match between
    the bitmap and the backbuffer, larger means easier to match but also easier to get a false positive.
    deformation_tolerance is how many pixels to search around a pixel to look for a match, this helps if
    images aren't pixel perfect. set closest to true if you only want the nearest match rather than an
    accurate match (this can produce nonsense easily). set x and y to the last output from find_bitmap
    to resume searching where the last search left off (re-entrant behaviour).
    
    :param bmp_id: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param color_tolerance: Optional[int], default = 0
    :param deformation_tolerance: Optional[int], default = 0
    :param percent_match: Optional[int], default = 99
    :param closest: Optional[bool], default = False
    :param x: Optional[int], default = 0
    :param y: Optional[int], default = 0
    :return: Union[bool, SimpleNamespace]
    """
    pass


def find_bitmap_spiral(
    bmp_id: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    color_tolerance: Optional[int] = 0,
    deformation_tolerance: Optional[int] = 0,
    x: Optional[int] = 0,
    y: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Find a bitmap identified by bmp_id in the current backbuffer (the current scrap()ed screen). Start
    searching in the center of the search region and continue searching outward in a spiral. If left,
    top, right, and/or bottom are specified then search only that region otherwise search the whole
    backbuffer. color_tolerance is how similar a color must be to match between the bitmap and the
    backbuffer, larger means easier to match but also easier to get a false positive.
    deformation_tolerance is how many pixels to search around a pixel to look for a match, this helps if
    images aren't pixel perfect. set closest to True if you only want the nearest match rather than an
    accurate match (this can produce nonsense easily). set x and y to the last output from find_bitmap
    to resume searching where the last search left off (re-entrant behaviour).
    
    :param bmp_id: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param color_tolerance: Optional[int], default = 0
    :param deformation_tolerance: Optional[int], default = 0
    :param x: Optional[int], default = 0
    :param y: Optional[int], default = 0
    :return: Union[bool, SimpleNamespace]
    """
    pass


def find_color(
    color: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    color_tolerance: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Find a pixel identified by color in the current backbuffer (the current scrap()ed screen). If left,
    top, right, and/or bottom are specified then search only that region otherwise search the whole
    backbuffer. color_tolerance is how similar a color must be to match between the bitmap and the
    backbuffer, larger means easier to match but also easier to get a false positive.
    
    :param color: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param color_tolerance: Optional[int], default = 0
    :return: Union[bool, SimpleNamespace]
    """
    pass


def find_color_spiral(
    color: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    color_tolerance: Optional[int] = 0,
    exclude_radius: Optional[int] = 0,
    x: Optional[int] = 0,
    y: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Find a pixel identified by color in the current backbuffer (the current scrap()ed screen). Start
    searching in the center of the search region and continue searching outward in a spiral. If left,
    top, right, and/or bottom are specified then search only that region otherwise search the whole
    backbuffer. color_tolerance is how similar a color must be to match between the bitmap and the
    backbuffer, larger means easier to match but also easier to get a false positive. set x and y to the
    last output from find_bitmap to resume searching where the last search left off (re-entrant
    behaviour).
    
    :param color: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param color_tolerance: Optional[int], default = 0
    :param exclude_radius: Optional[int], default = 0
    :param x: Optional[int], default = 0
    :param y: Optional[int], default = 0
    :return: Union[bool, SimpleNamespace]
    """
    pass


def find_text_using_font(
    font_id: int,
    text: str,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    minimum_spacing: Optional[int] = 0,
    maximum_spacing: Optional[int] = 30,
    color_tolerance: Optional[int] = 0,
    deformation_tolerance: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Specify a font_id returned from load_font_from_bitmap and text to search for which appears in the
    backbuffer in that font. A smaller sample of the desired text is advisible. Omit space characters.
    minimum_spacing is the minimum number of pixels between characters to allow when matching, typically
    this would be 0. maximum_spacing is the maximum number of pixels allowed between characters... if
    you wish to match a string containing spaces this should be at least as large as a space character,
    e.g. 30. color_tolerance is how similar a color must be to match between the bitmap and the
    backbuffer, larger means easier to match but also easier to get a false positive.
    deformation_tolerance is how many pixels to search around a pixel to look for a match, this helps if
    font characters aren't pixel perfect.
    
    :param font_id: int
    :param text: str
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param minimum_spacing: Optional[int], default = 0
    :param maximum_spacing: Optional[int], default = 30
    :param color_tolerance: Optional[int], default = 0
    :param deformation_tolerance: Optional[int], default = 0
    :return: Union[bool, SimpleNamespace]
    """
    pass


def create_encrypted(script: str, unique_code: str) -> str:
    """
    create_encrypted(script, unique_code) -- Create a script that can only be run on a specific PC,
    according to that PC's unique code.
    
    :param script: str
    :param unique_code: str
    :return: str
    """
    pass


def run_encrypted(script: str) -> None:
    """
    Run a script bound to this PC's unique code.
    
    :param script: str
    :return: None
    """
    pass


def unique_code() -> str:
    """
    Return this PC's unique code.
    
    :return: str
    """
    pass


def focus_game_window() -> None:
    """
    Send a focus message to the game or application window that has been selected.
    
    :return: None
    """
    pass


def unfocus_game_window() -> None:
    """
    Send an unfocus message to the game or application window that has been selected.
    
    :return: None
    """
    pass


def force_oversized_backbuffer(width: int, height: int) -> bool:
    """
    Resize the backbuffer to an arbitrary width and height. This can be used to paint a large image such
    as a map that you can then search with find_bitmap etc.
    
    :param width: int
    :param height: int
    :return: bool
    """
    pass


def get_ancestor_window_hwnd() -> int:
    """
    Returns the window handle of the top level window.
    
    :return: int
    """
    pass


def get_async_key_state(virtual_key: int) -> int:
    """
    Determines whether a key is up or down at the time the function is called, and whether the key was
    pressed after a previous call to GetAsyncKeyState. Direct port of Windows API: GetAsyncKeyState
    (google this for more info).
    
    :param virtual_key: int
    :return: int
    """
    pass


def get_backbuffer_height() -> int:
    """
    Returns the height in pixels of the current backbuffer.
    
    :return: int
    """
    pass


def get_backbuffer_width() -> int:
    """
    Returns the width in pixels of the current backbuffer.
    
    :return: int
    """
    pass


def get_bitmap_height(bmp_id: int) -> int:
    """
    Returns the height in pixels of the specified bitmap.
    
    :param bmp_id: int
    :return: int
    """
    pass


def get_bitmap_width(bmp_id: int) -> int:
    """
    Returns the width in pixels of the specified bitmap.
    
    :param bmp_id: int
    :return: int
    """
    pass


def get_color(x: int, y: int) -> int:
    """
    Returns the RGB color at the specified pixel location in the backbuffer.
    
    :param x: int
    :param y: int
    :return: int
    """
    pass


def get_font_height(bmp_id: int) -> int:
    """
    Returns the height in pixels of the specified font.
    
    :param bmp_id: int
    :return: int
    """
    pass


def get_game_dims() -> SimpleNamespace:
    """
    Returns the width and height of the target game or application.
    
    :return: SimpleNamespace
    """
    pass


def get_game_rect() -> SimpleNamespace:
    """
    Returns the left, top, right, bottom of the selected window's drawing surface relative to the top
    left pixel of the desktop.
    
    :return: SimpleNamespace
    """
    pass


def get_game_title() -> str:
    """
    Returns the title of the target game or application.
    
    :return: str
    """
    pass


def get_height() -> int:
    """
    Returns the height of the target window's draw surface.
    
    :return: int
    """
    pass


def get_inner_window_class_name_by_hwnd(hwnd: int) -> Union[bool, str]:
    """
    Returns the class name for an inner window's handle.
    
    :param hwnd: int
    :return: Union[bool, str]
    """
    pass


def get_inner_window_dims(hwnd: int) -> Union[bool, SimpleNamespace]:
    """
    Returns the left, top, width and height of a specified inner window handle's drawing surface.
    
    :param hwnd: int
    :return: Union[bool, SimpleNamespace]
    """
    pass


def get_inner_window_from_ancestor_point(x: int, y: int) -> int:
    """
    Returns the window handle of an inner window under the specified point relative to the ancestor
    window.
    
    :param x: int
    :param y: int
    :return: int
    """
    pass


def get_inner_window_from_point(x: int, y: int) -> int:
    """
    Returns the window handle of an inner window under the specified point relative to the currently
    selected inner window.
    
    :param x: int
    :param y: int
    :return: int
    """
    pass


def get_inner_window_text_by_hwnd(hwnd: int) -> int:
    """
    Returns the title of an inner window according to window handle... this would typically be blank in
    all but a few special cases.
    
    :param hwnd: int
    :return: int
    """
    pass


def get_last_mouse_x() -> int:
    """
    Returns the x coordinate, relative to selected inner window, of the last mouse action performed by
    libsythe.
    
    :return: int
    """
    pass


def get_last_mouse_y() -> int:
    """
    Returns the y coordinate, relative to selected inner window, of the last mouse action performed by
    libsythe.
    
    :return: int
    """
    pass


def get_modules() -> dict:
    """
    Returns a list of DLLs loaded into the target application.
    
    :return: dict
    """
    pass


def get_poly_point(
    color: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    color_tolerance: Optional[int] = 30,
    gap_tolerance: Optional[int] = 10,
    margin: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Given some sort of closed ring-like shape of the specified color, found anywhere inside the
    backbuffer or within the region specified by left top right bottom if these are specified, search
    starting from the center of the search region, and when such a ring-like object is found return a
    random point inside that object. color_tolerance is how similar a color must be to match between the
    bitmap and the backbuffer, larger means easier to match but also easier to get a false positive.
    gap_tolerance is how much of a gap you are willing to tolerate in such a ring-like object (in
    pixels). margin is how many pixels from the outside of the ring to leave (avoid) when picking a
    random inner point.
    
    :param color: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param color_tolerance: Optional[int], default = 30
    :param gap_tolerance: Optional[int], default = 10
    :param margin: Optional[int], default = 0
    :return: Union[bool,SimpleNamespace]
    """
    pass


def get_width() -> int:
    """
    Returns the width of the target window's draw surface.
    
    :return: int
    """
    pass


def get_window_hwnd() -> int:
    """
    Return the window handle of the currently selected inner window, or if no inner window is selected
    then the ancestor window.
    
    :return: int
    """
    pass


def is_color(color_a: int, color_b: int, color_tolerance: int) -> bool:
    """
    Compares color_a against color_b according to a color distance computation. Iff both colors are
    within the color_tolerance specified then the result is true.
    
    :param color_a: int
    :param color_b: int
    :param color_tolerance: int
    :return: bool
    """
    pass


def is_inner_window(hwnd: int) -> bool:
    """
    Returns true iff the specified window handle is an inner window of the currently selected ancestor
    window.
    
    :param hwnd: int
    :return: bool
    """
    pass


def key_down(scan_code: int) -> None:
    """
    Returns true iff the key specified by scan_code is currently down. For a list of scan_codes google
    VK virtual keys, or refer to VK_ constants provided by the library.
    
    :param scan_code: int
    :return: None
    """
    pass


def key_up(scan_code: int) -> None:
    """
    Returns true iff the key specified by scan_code is currently not down. For a list of scan_codes
    google VK virtual keys, or refer to VK_ constants provided by the library.
    
    :param scan_code: int
    :return: None
    """
    pass


def load_bitmap_from_backbuffer(
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    transparent_color: Optional[int] = 0,
    is_mask: Optional[bool] = False,
    override_bmp_id: Optional[int] = 0,
) -> int:
    """
    Given a region specified by left top right bottom create a bitmap. transparent_color if specified is
    the RGB color to be regarded as transparent when creating the bitmap. is_mask specifies if this is a
    mask... masks are bitmaps but they can match any object of solid color (of any color) that is the
    same shape as the mask. override_bmp_id if unspecified or 0 will generate a new bitmap id for this
    bitmap, but if specified will override the specified bitmap id with the newly generated bitmap, this
    prevents memory exhaustion when using this function in a loop.
    
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param transparent_color: Optional[int], default = 0
    :param is_mask: Optional[bool], default = False
    :param override_bmp_id: Optional[int], default = 0
    :return: int
    """
    pass


def load_bitmap_from_bytes(
    byte_array: bytes,
    transparent_color: Optional[int] = 0,
    is_mask: Optional[bool] = False,
    strip_alpha: Optional[bool] = False,
) -> int:
    """
    byte_array is a bytes-like object that contains the octet data for some sort of image in some sort
    of common format such as windows bmp or png or jpg. transparent_color if specified is the RGB color
    to be regarded as transparent when creating the bitmap. is_mask specifies if this is a mask... masks
    are bitmaps but they can match any object of solid color (of any color) that is the same shape as
    the mask. strip_alpha when set to true will convert any pixel containing a non-zero alpha channel to
    the transparent_color specified.
    
    :param byte_array: bytes
    :param transparent_color: Optional[int], default = 0
    :param is_mask: Optional[bool], default = False
    :param strip_alpha: Optional[bool], default = False
    :return: int
    """
    pass


def load_bitmap_from_file(
    file_name: str,
    transparent_color: Optional[int] = 0,
    is_mask: Optional[bool] = False,
) -> int:
    """
    file_name is a file local to the current working directory of the script that contains the octet
    data for some sort of image in some sort of common format such as windows bmp or png or jpg.
    transparent_color if specified is the RGB color to be regarded as transparent when creating the
    bitmap. is_mask specifies if this is a mask... masks are bitmaps but they can match any object of
    solid color (of any color) that is the same shape as the mask.
    
    :param file_name: str
    :param transparent_color: Optional[int], default = 0
    :param is_mask: Optional[bool], default = False
    :return: int
    """
    pass


def load_bitmap_from_string(
    bmp_string: str,
    transparent_color: Optional[int] = 0,
    is_mask: Optional[bool] = False,
) -> int:
    """
    bmp_string is a string previously produced by libsythe that contains the base85 encoded octet data
    for a compressed bitmap. transparent_color if specified is the RGB color to be regarded as
    transparent when creating the bitmap. is_mask specifies if this is a mask... masks are bitmaps but
    they can match any object of solid color (of any color) that is the same shape as the mask.
    
    :param bmp_string: str
    :param transparent_color: Optional[int], default = 0
    :param is_mask: Optional[bool], default = False
    :return: int
    """
    pass


def load_font_from_bitmap(
    bmp_id: int, characters: str, transparent_color: Optional[int] = 0
) -> int:
    """
    Loads a font for use in the ocr functions. bmp_id is a bitmap previously loaded that contains a mask
    of the characters in the font. characters is a string containing each of the characters in the mask
    in sequence. transparent_color should be the same as is set inside the bitmap the font is loaded
    from.
    
    :param bmp_id: int
    :param characters: str
    :param transparent_color: Optional[int], default = 0
    :return: int
    """
    pass


def load_mask_from_bytes(
    byte_array: bytes,
    transparent_color: Optional[int] = 0,
    is_mask: Optional[bool] = 0,
    strip_alpha: Optional[bool] = False,
) -> int:
    """
    load_bitmap_from_bytes(byte_array, transparent_color, is_mask, strip_alpha) --  byte_array is a
    bytes-like object that contains the octet data for some sort of image in some sort of common format
    such as windows bmp or png or jpg. transparent_color if specified is the RGB color to be regarded as
    transparent when creating the bitmap. is_mask specifies if this is a mask... masks are bitmaps but
    they can match any object of solid color (of any color) that is the same shape as the mask.
    strip_alpha when set to true will convert any pixel containing a non-zero alpha channel to the
    transparent_color specified.
    
    :param byte_array: bytes
    :param transparent_color: Optional[int], default = 0
    :param is_mask: Optional[bool], default = 0
    :param strip_alpha: Optional[bool], default = False
    :return: int
    """
    pass


def load_mask_from_file(
    file_name: str,
    transparent_color: Optional[int] = 0xFF00FF,
    is_mask: Optional[bool] = True,
) -> int:
    """
    load_bitmap_from_file(file_name, transparent_color, is_mask) --  file_name is a file local to the
    current working directory of the script that contains the octet data for some sort of image in some
    sort of common format such as windows bmp or png or jpg. transparent_color if specified is the RGB
    color to be regarded as transparent when creating the bitmap. is_mask specifies if this is a mask...
    masks are bitmaps but they can match any object of solid color (of any color) that is the same shape
    as the mask.
    
    :param file_name: str
    :param transparent_color: Optional[int], default = 0xff00ff
    :param is_mask: Optional[bool], default = True
    :return: int
    """
    pass


def load_mask_from_string(
    bmp_string: str,
    transparent_color: Optional[int] = 0xFF00FF,
    is_mask: Optional[bool] = True,
) -> int:
    """
    load_bitmap_from_string(bmp_string, transparent_color, is_mask) --  bmp_string is a string
    previously produced by libsythe that contains the base85 encoded octet data for a compressed bitmap.
    transparent_color if specified is the RGB color to be regarded as transparent when creating the
    bitmap. is_mask specifies if this is a mask... masks are bitmaps but they can match any object of
    solid color (of any color) that is the same shape as the mask.
    
    :param bmp_string: str
    :param transparent_color: Optional[int], default = 0xff00ff
    :param is_mask: Optional[bool], default = True
    :return: int
    """
    pass


def mouse_button_down(x: int, y: int, button: int) -> None:
    """
    Set the mouse button to down at the specified location. button = 0 is left, button = 1 is right,
    button = 2 is middle.
    
    :param x: int
    :param y: int
    :param button: int
    :return: None
    """
    pass


def mouse_button_up(x: int, y: int, button: int) -> None:
    """
    Set the mouse button. button = 0 is left, button = 1 is right, button = 2 is middle.
    
    :param x: int
    :param y: int
    :param button: int
    :return: None
    """
    pass


def move_mouse(x: int, y: int) -> bool:
    """
    Move mouse, real or virtual depending on what is set prior to the function call to the specified
    coordinates.
    
    :param x: int
    :param y: int
    :return: bool
    """
    pass


def next_inner_windows_iterator() -> int:
    """
    Return the next window handle in the hierarchy of inner window handles of the currently selected
    window.
    
    :return: int
    """
    pass


def ocr_using_font(
    font_id: int,
    left: Optional[int] = 0,
    top: Optional[int] = 0,
    right: Optional[int] = -1,
    bottom: Optional[int] = -1,
    minimum_spacing: Optional[int] = 0,
    maximum_spacing: Optional[int] = 30,
    color_tolerance: Optional[int] = 50,
    deformation_tolerance: Optional[int] = 0,
) -> Union[bool, SimpleNamespace]:
    """
    Specify a font_id returned from load_font_from_bitmap whose characters you wish to search for in the
    backbuffer. Returns the text found and the location it was found. Space characters are omitted from
    the found text. Only a single line of text will be returned (the top-left most in the search area).
    If multiple lines of text are required please increment the top parameter by the return value of
    get_font_height(font_id) and search againt to get the next line. minimum_spacing is the minimum
    number of pixels between characters to allow when matching, typically this would be 0.
    maximum_spacing is the maximum number of pixels allowed between characters... if you wish to match a
    string containing spaces this should be at least as large as a space character, e.g. 30.
    color_tolerance is how similar a color must be to match between the bitmap and the backbuffer,
    larger means easier to match but also easier to get a false positive. deformation_tolerance is how
    many pixels to search around a pixel to look for a match, this helps if font characters aren't pixel
    perfect.
    
    :param font_id: int
    :param left: Optional[int], default = 0
    :param top: Optional[int], default = 0
    :param right: Optional[int], default = -1
    :param bottom: Optional[int], default = -1
    :param minimum_spacing: Optional[int], default = 0
    :param maximum_spacing: Optional[int], default = 30
    :param color_tolerance: Optional[int], default = 50
    :param deformation_tolerance: Optional[int], default = 0
    :return: Union[bool,SimpleNamespace]
    """
    pass


def ready() -> bool:
    """
    Iff a valid window has been set using set_window this function will return true.
    
    :return: bool
    """
    pass


def reset_inner_windows_iterator() -> None:
    """
    Reset the iterator used by next_inner_windows_iterator back to the start.
    
    :return: None
    """
    pass


def save_bitmap_to_file(bmp_id: int, file_name: str) -> bool:
    """
    Given a valid bitmap indicated by bmp_id save it to a file specified by file_name in the current
    working directory.
    
    :param bmp_id: int
    :param file_name: str
    :return: bool
    """
    pass


def scrape() -> None:
    """
    Copy ALL of the pixels in the target application to the backbuffer, resizing the backbuffer as
    needed to accomodate those pixels. The method by which pixels are copied depend on the setup
    functions called before scrape() is called. By default window's desktop replication api is used. If
    this is resulting in other windows getting 'in between' your target application and the scraper then
    consider using gdi with set_gdi_capture_on() which you should call after selecting outer and inner
    windows.
    
    :return: None
    """
    pass


def scrape_rect(left: int, top: int, width: int, height: int) -> None:
    """
    Copy only the pixels inside the specified reigon of the target application to the backbuffer. The
    backbuffer will be resized to the current full size of the target window. The target region will be
    copied 'in place' from the current window to the same position on the backbuffer. The method by
    which pixels are copied depend on the setup functions called before scrape() is called. By default
    window's desktop replication api is used. If this is resulting in other windows getting 'in between'
    your target application and the scraper then consider using gdi with set_gdi_capture_on() which you
    should call after selecting outer and inner windows.
    
    :param left: int
    :param top: int
    :param width: int
    :param height: int
    :return: None
    """
    pass


def select_inner_window_by_hwnd(hwnd: int) -> bool:
    """
    Set inner window by window handle.
    
    :param hwnd: int
    :return: bool
    """
    pass


def select_inner_window(class_name: str) -> bool:
    """
    Set inner window by class name.
    
    :param class_name: str
    :return: bool
    """
    pass


def select_inner_window_exact(class_name: str) -> bool:
    """
    Set inner window by class name without partial matches allowed.
    
    :param class_name: str
    :return: bool
    """
    pass


def select_inner_window_from_ancestor_point(x: int, y: int) -> bool:
    """
    Select inner window according to a point relative to the top left of the client area of the ancestor
    window (the window set with set_window).
    
    :param x: int
    :param y: int
    :return: bool
    """
    pass


def select_inner_window_from_point(x: int, y: int) -> bool:
    """
    Select inner window according to a point relative to the top left of the currently selected inner
    window, or if no inner window is currently selected then the ancestor window.
    
    :param x: int
    :param y: int
    :return: bool
    """
    pass


def select_inner_window_regex(class_name_regex: str) -> bool:
    """
    Select inner window according to a class name but use a regular expression.
    
    :param class_name_regex: str
    :return: bool
    """
    pass


def send_alt_key(key: int) -> None:
    """
    Sends ALT+key where key is a virtual key as described by the VK_ constants in this library.
    
    :param key: int
    :return: None
    """
    pass


def send_keys(keys: str) -> None:
    """
    Send a sequence of characters as described in the keys string. Useful for inputing standard ASCII
    data.
    
    :param keys: str
    :return: None
    """
    pass


def set_color(x: int, y: int, color: int) -> None:
    """
    Debugging function. Slow. Sets a pixel on the physical screen of the computer at the x,y position
    according to the top left of the currently selected window.
    
    :param x: int
    :param y: int
    :param color: int
    :return: None
    """
    pass


def set_gdi_capture_off() -> None:
    """
    Turn GDI capture mode off.
    
    :return: None
    """
    pass


def set_gdi_capture_on() -> None:
    """
    Turn GDI capture mode on. This is a slow but effective mode for capturing many applications and can
    allow the application to be behind other windows without capturing the foreground windows.
    
    :return: None
    """
    pass


def set_human_mouse_mode_off() -> None:
    """
    Turns off human mouse mode.
    
    :return: None
    """
    pass


def set_human_mouse_mode_on() -> None:
    """
    On by default. Mouse, virtual or real, follows a randomly generated human-like trail according to
    complex mathematics.
    
    :return: None
    """
    pass


def set_mouse_delay(us_delay: int, us_delay_random: Optional[int]) -> None:
    """
    Adds a delay to mouse movement. Values are in microseconds, default 500us
    
    :param us_delay: int
    :param us_delay_random: Optional[int]
    :return: None
    """
    pass


def set_virtual_inputs_off() -> None:
    """
    Force libsythe to use the physical mouse. Useful for debugging and target games or applications that
    ignore window messages. However very annoying for the end user as their physical mouse will be
    essentially unusable during the time the script is executing.
    
    :return: None
    """
    pass


def set_virtual_inputs_on() -> None:
    """
    On by default. Use a virtual mouse communicated to the target game or application via window
    messages.
    
    :return: None
    """
    pass


def set_virtual_mouse_java_mode_off() -> None:
    """
    Turns java mode off.
    
    :return: None
    """
    pass


def set_virtual_mouse_java_mode_on() -> None:
    """
    If set_virtual_inputs_on() then you may also specify this to deliver a modified window message to
    the target application where the target application is a java application using AWT. This modifier
    corrects for a very long standing bug in the way AWT's c++ code is written.
    
    :return: None
    """
    pass


def set_virtual_mouse_wfp_mode_off() -> None:
    """
    Turn off window-from-point mode.
    
    :return: None
    """
    pass


def set_virtual_mouse_wfp_mode_on() -> None:
    """
    Turn on window-from-point mode. It is a virtual mouse mode that is incompatible with java mode. In
    this mode libsythe will attempt to drill down to the bottom most inner window below the target point
    of a mouse movement or click and attempt to send window messages directly to that inner most window.
    For some native windows applications such as mspaint this works very well. For other applications it
    may cause problems. Occasionally you may need to turn it on and off periodically inside your script
    depending on the task.
    
    :return: None
    """
    pass


def set_window_by_hwnd(hwnd: int) -> Union[bool, str]:
    """
    Set ancestor window according to a window handle.
    
    :param hwnd: int
    :return: Union[bool,str]
    """
    pass


def set_window_by_title_and_class_regex(
    title_regex: str, class_regex: str
) -> Union[bool, str]:
    """
    Set an ancestor window according to both a title and a class regular expression.
    
    :param title_regex: str
    :param class_regex: str
    :return: Union[bool,str]
    """
    pass


def set_window_by_title(title: str) -> Union[bool, str]:
    """
    Set ancestor window according to partial match of window title.
    
    :param title: str
    :return: Union[bool,str]
    """
    pass


def set_window_by_title_regex(title_regex: str) -> Union[bool, str]:
    """
    Set ancestor window according to regular expression match of window title.
    
    :param title_regex: str
    :return: Union[bool,str]
    """
    pass


def set_window(title: str) -> Union[bool, str]:
    """
    Set ancestor window according to partial match of window title.
    
    :param title: str
    :return: Union[bool,str]
    """
    pass


def set_wmprint_off() -> None:
    """
    Turn off WM Print mode.
    
    :return: None
    """
    pass


def set_wmprint_on() -> None:
    """
    Turn on WM Print mode. In this mode scrape() attempts to pull the pixels from the target window by
    having it 'print' into a virtual printer. This is a very old window mesasge from the days of windows
    3.1 and would not be useful except in very limited circumstances where GDI and desktop duplication
    api both do not work.
    
    :return: None
    """
    pass


def show_bitmap_picker() -> SimpleNamespace:
    """
    Show a dragable window that selects a bitmap from the current window.
    
    :return: SimpleNamespace
    """
    pass


def show_color_picker() -> SimpleNamespace:
    """
    Show a hovering window that selects a color (pixel) from the current window.
    
    :return: SimpleNamespace
    """
    pass


def show_game_window(maximize_window: bool) -> None:
    """
    Bring game window to front and optionally maximise it.
    
    :param maximize_window: bool
    :return: None
    """
    pass


def show_inner_window_picker() -> None:
    """
    Show a hovering window that allows selection of an inner window from the point under the cursor.
    
    :return: None
    """
    pass


def sleep(milliseconds: int) -> None:
    """
    Pause execution for a specified number of milliseconds without wasting CPU.
    
    :param milliseconds: int
    :return: None
    """
    pass


def unselect_inner_window() -> None:
    """
    Deselect the current inner window, reverts to the ancestor window.
    
    :return: None
    """
    pass
