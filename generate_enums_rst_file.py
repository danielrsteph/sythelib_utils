import re
import textwrap
from collections import defaultdict

ARGUMENT_RE = re.compile(r"^([A-Z0-9_]+) = \d{1,4}")


def make_header_and_hlist(header, items):
    category_lines = [
        header,
        "=" * len(header),
        "\n.. hlist::",
        textwrap.indent(":columns: 3\n", "    "),
    ]
    category_lines.extend(
        filter(
            lambda x: x is not None,
            [textwrap.indent(f"* {item}", "    ") for item in items if item],
        )
    )
    return category_lines


def create_enum_file(file_name):
    names = file_name.split('_')
    names = [name.capitalize() for name in names]
    name = "".join(names)

    rst_lines = ["===========", f"{name} Enums Index", "===========\n"]

    with open(f"sythelib_utils/{file_name}_enums.py") as fp:
        enums = fp.read()
    enums = [line.strip() for line in enums.split("\n")]
    matches = [ARGUMENT_RE.findall(line) for line in enums]

    categories = defaultdict(list)
    for elem in matches:
        if elem:
            categories[elem[0][0].upper()].append(elem[0])

    ordered_categories = dict(sorted(categories.items()))
    for k, v in ordered_categories.items():
        ordered_categories[k] = sorted(v)

    for header, enum in ordered_categories.items():
        rst_lines.extend(make_header_and_hlist(header, enum))
        rst_lines.append("\n")

    with open(f"./docs/source/{file_name.replace('_', '-')}-index.rst", "w") as fp:
        fp.write("\n".join(rst_lines))


if __name__ == "__main__":
    file_names = ["var_player", "var_bit"]
    for file_name in file_names:
        create_enum_file(file_name)
