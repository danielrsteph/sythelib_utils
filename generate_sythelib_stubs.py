"""
This file generates a fake pylibsythe.py file so that the docs generation can work
on a Linux machine as opposed to a windows one - as a .py file is importable on Linux.

This file is not shipped with the package when pip installed and neither is the generated
.py file. Any time changes are made to the real pysythelib.pyi or .pyd files the pysythelib.py
file must be regenerated. Contact thomm.o#8637 on discord if you need help doing this.
"""
import textwrap
import types
import os
import re
import typing

FUNCTION_RE = re.compile(
    r"^def\s(?P<name>[a-zA-Z_]+)\((?P<args>[^)]*)\)\s*(?:->\s*(?P<returntype>[^:]+)):$"
)
ARGUMENT_RE = re.compile(
    r"(?P<arg>[^:, ]+)(?::\s*(?P<annotation>[^,= ]+))(?:\s*=\s*(?P<default>[^,)]+))?"
)


class Argument(typing.NamedTuple):
    name: str
    annotation: str
    default: str


def remove_previous_file():
    try:
        os.remove("pylibsythe.py")
    except FileNotFoundError:
        pass


def build_stubs_for_func(func, args, return_annotation):
    if not isinstance(func, (types.BuiltinFunctionType, types.BuiltinMethodType)):
        return None
    param_strings = []
    if args:
        parsed_args = [
            Argument(name, annotation, default)
            for name, annotation, default in ARGUMENT_RE.findall(args)
        ]
        param_strings.extend(
            [
                f"\n:param {arg.name}: {arg.annotation}{f', default = {arg.default}' if arg.default else ''}"
                for arg in parsed_args
            ]
        )
    docstring = '"""\n{}\n{}{}"""\npass'.format(
        "\n".join(textwrap.wrap(func.__doc__, 100)),
        "".join(param_strings),
        f"\n:return: {return_annotation}\n",
    )
    return f"""def {func.__name__}({args}) -> {return_annotation}:\n{textwrap.indent(docstring, '    ', lambda l: True)}\n\n"""


def write_stubs_to_file(stubs):
    with open("pylibsythe.py", "w") as fp:
        fp.write(stubs)


if __name__ == "__main__":
    remove_previous_file()
    import pylibsythe

    stubs = []

    path = os.path.join(os.path.abspath(pylibsythe.__file__), "..", "pylibsythe.pyi")
    with open(path) as fp:
        pyi_file = fp.read()
        pyi_file = pyi_file.replace("...", "")
    definitions = [line.strip() for line in pyi_file.split("\n")]
    stubs.append(definitions.pop(0))
    stubs.append(f"{definitions.pop(0)}, Optional")
    stubs.append("\n")

    for definition in definitions:
        if match := FUNCTION_RE.match(definition):
            stubs.append(
                build_stubs_for_func(
                    getattr(pylibsythe, match.group("name")),
                    match.group("args"),
                    match.group("returntype"),
                )
            )

    stubs = "\n".join(stub for stub in stubs if stub is not None)
    write_stubs_to_file(stubs)
