#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
import typing
from unittest import mock

from sythelib_utils.items import Items
from sythelib_utils.font import Fonts
from sythelib_utils.custom_mouse import random_move_mouse

from sythelib_utils import utils
from sythelib_utils import singleton
from sythelib_utils import tabs
from sythelib_utils import anti_ban
from sythelib_utils import colors
from sythelib_utils import var_bit_enums
from sythelib_utils import var_player_enums
from sythelib_utils import plugin_client
from sythelib_utils import models

from sythelib_utils.utils import *
from sythelib_utils.singleton import *
from sythelib_utils.anti_ban import *
from sythelib_utils.inventory import *
from sythelib_utils.colors import *
from sythelib_utils.var_player_enums import *
from sythelib_utils.var_bit_enums import *
from sythelib_utils.models import *
from sythelib_utils.plugin_client import *

if typing.TYPE_CHECKING:
    FONTS: typing.Final[Fonts] = mock.MagicMock()
    """The cached RuneScape fonts. This is an instance of the :obj:`~.font.Fonts` class."""
    ITEMS: typing.Final[Items] = mock.MagicMock()
    """The cached RuneScape items. This is an instance of the :obj:`~.items.Items` class."""
    client: typing.Final[PluginClient] = mock.MagicMock()
    """The Plugin Client which interacts with the API. This is an instance of the :obj:`~.items.PluginClient` class."""
else:
    FONTS: typing.Final[Fonts] = Fonts()
    ITEMS: typing.Final[Items] = Items()
    client: typing.Final[PluginClient] = PluginClient()

__version__ = "1.0.2"

__all__: typing.Final[typing.List[str]] = [
    "FONTS",
    "ITEMS",
    "random_move_mouse",
    *inventory.__all__,
    *utils.__all__,
    *singleton.__all__,
    *anti_ban.__all__,
    *colors.__all__,
    *models.__all__,
    *plugin_client.__all__,
]
