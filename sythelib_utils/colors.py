#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = [
    "to_hex",
    "to_rgb",
    "to_int",
    "average_color",
    "largest_distance",
    "color_distance",
    "color_range",
    "find_colors",
]

import math
import typing
from types import SimpleNamespace

from pylibsythe import *


def to_hex(value: int) -> str:
    """
    Converts an int hexadecimal

    :param value: int
        the int to convert to hex
    :return: str
        the string representation of the hexadecimal
    """
    return "0x%02x%02x%02x" % to_rgb(value)


def to_rgb(color: int) -> typing.Tuple[int, int, int]:
    """
    Converts an int to rgb values

    :param color: int
        the color to convert
    :return: typing.Tuple[int, int, int]
        the rgb values in order of rgb
    """
    return (color >> 16) & 0xFF, (color >> 8) & 0xFF, color & 0xFF


def to_int(color: typing.Tuple[int, int, int]) -> int:
    """
    Converts a color to an int

    :param color: typing.Tuple[int, int, int]
        the rgb value of the color
    :return: int
        the color in int format
    """
    r, g, b = color
    return (r << 16) + (g << 8) + b


def average_color(*colors: typing.Tuple[int, int, int]) -> typing.Tuple[int, int, int]:
    """
    Finds the average color

    :param colors: int
        the colors to average
    :return: typing.Tuple[int, int, int]
        the average rgb values in order of rgb
    """
    r_t, g_t, b_t = 0, 0, 0
    length = len(colors)
    for r, g, b in colors:
        r_t += r
        g_t += g
        b_t += b
    return r_t // length, g_t // length, b_t // length


def largest_distance(
    color: typing.Tuple[int, int, int], *colors: typing.Tuple[int, int, int]
) -> int:
    """
    Finds the largest distance between a given color and colors

    :param color: int
        the color to compare with
    :param colors: int
        the colors to compare to
    :return: int
        the largest distance
    """
    return max(color_distance(color, c) for c in colors)


def color_distance(
    color1: typing.Tuple[int, int, int], color2: typing.Tuple[int, int, int]
) -> int:
    """
    Calculates the distance between two colors of equal modes

    :param color1: typing.Tuple[int, int, int]
        first colour to compare
    :param color2: typing.Tuple[int, int, int]
        second colour to compare
    :return:
    """
    return sum((x - y) ** 2 for x, y in zip(color1, color2)) ** 0.5


def color_range(
    *colors: typing.Tuple[int, int, int]
) -> typing.Tuple[typing.Tuple[int, int, int], int]:
    """
    Finds the color range (average color and tolerance) which will
    encapsulate all given colors

    :param colors: int
        The colors to average and find the distance of
    :return: typing.Tuple[typing.Tuple[int, int, int], int]
        A tuple containing the rgb average color and the tolerance to
        include all the colors
    """
    average = average_color(*colors)
    return average, math.ceil(largest_distance(average, *colors))


def find_colors(
    color: int,
    left: typing.Optional[int] = 0,
    top: typing.Optional[int] = 0,
    right: typing.Optional[int] = None,
    bottom: typing.Optional[int] = None,
    color_tolerance: typing.Optional[int] = 0,
) -> typing.List[SimpleNamespace]:
    """
    Finds all the colors in the current backbuffer

    :param color: int
        the color to search for
    :param left: int
        the left coordinate
    :param top: int
        the top coordinate
    :param right: int
        the right coordinate
    :param bottom: int
        the bottom coordinate
    :param color_tolerance: int
        the color tolerance to search with
    :return: typing.List[SimpleNamespace]
        the coordinates of the found colors
    """
    colors: typing.List[SimpleNamespace] = []
    right, bottom = right or (get_width() - 1), bottom or (get_width() - 1)
    scrape()

    found_color = find_color_spiral(color, left, top, right, bottom, color_tolerance)
    if found_color:
        x, y = found_color.y, found_color.x
        colors.append(found_color)

        while found_color := find_color_spiral(
            color, left, top, right, bottom, color_tolerance, x=x, y=y
        ):

            if found_color not in colors:
                colors.append(found_color)
                x, y = found_color.y, found_color.x
            else:
                return colors

    return colors
