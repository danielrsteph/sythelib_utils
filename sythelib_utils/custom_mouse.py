#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["random_move_mouse"]

import math
import random
import time

import typing

from pylibsythe import move_mouse, sleep, get_last_mouse_x, get_last_mouse_y

SPEED: typing.Final[int] = 16
GRAVITY: typing.Final[float] = 8 + random.random() / 2
WIND: typing.Final[float] = 4 + random.random() / 2


def speed() -> int:
    return SPEED


def gravity() -> float:
    return GRAVITY + random.random() / 2


def wind() -> float:
    return WIND


def _wind_mouse(
    xs,
    ys,
    xe,
    ye,
    gravity: float,
    wind: float,
    min_wait: float,
    max_wait: float,
    max_step: float,
    target_area: float,
) -> None:
    (
        velo_x,
        velo_y,
        wind_x,
        wind_y,
        velo_mag,
        dist,
        random_dist,
        step,
        max_step_start,
    ) = (0, 0, 0, 0, 0, 0, 0, 0, max_step)
    t: float = time.perf_counter() + 1.5

    while (dist := math.hypot(xs - xe, ys - ye)) > 1:
        if time.perf_counter() > t:
            raise TimeoutError("Mouse timed out")

        if dist > max_step_start / 2:
            max_step = max_step_start

        wind = min(wind, dist)

        if dist >= target_area:
            wind_x = wind_x / math.sqrt(3) + (
                random.random() * (round(wind) * 2 + 1) - wind
            ) / math.sqrt(5)
            wind_y = wind_y / math.sqrt(3) + (
                random.random() * (round(wind) * 2 + 1) - wind
            ) / math.sqrt(5)

        else:
            wind_x = wind_x / math.sqrt(2)
            wind_y = wind_y / math.sqrt(2)
            max_step = (
                random.random() * 3 + 3.0 if max_step < 3 else max_step / math.sqrt(5)
            )

        velo_x += wind_x + gravity * (xe - xs) / dist
        velo_y += wind_y + gravity * (ye - ys) / dist

        if math.hypot(velo_x, velo_y) > max_step:
            random_dist = max_step / 2.0 + random.random() * (round(max_step) // 2)
            velo_mag = math.sqrt(velo_x * velo_x + velo_y * velo_y)
            velo_x = (velo_x / velo_mag) * random_dist
            velo_y = (velo_y / velo_mag) * random_dist

        last_x = round(xs)
        last_y = round(ys)
        xs = last_x + velo_x
        ys = last_y + velo_y

        if last_x != round(xs) or last_y != round(ys):
            move_mouse(round(xs), round(ys))

        step = math.hypot(xs - last_x, ys - last_y)
        sleep(round((max_wait - min_wait) * (step / max_step) + min_wait))
    move_mouse(round(xe), round(ye))


def random_move_mouse(x: int, y: int) -> None:
    """
    Randomly moves the mouse based on Simba's mouse movements. Follows a randomized path to the point.

    :param x: int
        the x coordinate
    :param y: int
        the y coordinate
    :return: None
    """
    speed_, gravity_, wind_ = speed(), gravity(), wind()
    rand_speed = (random.random() * speed_ / 2.0 + speed_) / 10.0
    _wind_mouse(
        get_last_mouse_x(),
        get_last_mouse_y(),
        x,
        y,
        gravity_,
        wind_,
        10.0 / rand_speed,
        15.0 / rand_speed,
        10.0 * rand_speed,
        10.0 * rand_speed,
    )
