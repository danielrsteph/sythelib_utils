#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = [
    "scrape_inventory",
    "inventory_slots",
    "get_inventory_slot",
    "is_inventory_full",
    "search_inventory",
    "drop_inventory_slots",
    "drop_items",
    "is_slot_full",
    "get_empty_slots",
    "DROP_PATTERN_REGULAR",
    "DROP_PATTERN_REGULAR_R",
    "DROP_PATTERN_SNAKE",
    "DROP_PATTERN_SNAKE_R",
    "DROP_PATTERN_TOPDOWN",
    "DROP_PATTERN_TOPDOWN_R",
    "DROP_PATTERN_SPIRAL",
    "DROP_PATTERN_ALTERNATING",
]

import random
import typing
from types import SimpleNamespace

from pylibsythe import *

from sythelib_utils.lazy_load_bitmap_from_string import LazyLoadBitmapFromString
from sythelib_utils.utils import click_random_circle_point

_empty_inventory = LazyLoadBitmapFromString(
    "0001_022TcLRx4sF+o`-Q&~wzkR$*Aaf|>8Ra0p&0O5hBrlmsy5C8x|i9j-S-%|L?6`_=5FPBXrC>A7;GEHU3T5jqjp`%eSpipZVF~+s2VyekoqXD"
    "`A(P-s1(hxA4wo=C6*?}bs2)LPv0#Yt_4>^Dww#iyZl9MpQH+LqMfyvsoK(iCdAbLQ3!>|kSK~JcG()57Jr6Jmao&OhdML1B9Nk@<*00000"
)

DROP_PATTERN_REGULAR: typing.Final[typing.Tuple[int, ...]] = (
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
)
"""Dropping left to right top from bottom"""

DROP_PATTERN_REGULAR_R: typing.Final[typing.Tuple[int, ...]] = (
    3,
    2,
    1,
    0,
    7,
    6,
    5,
    4,
    11,
    10,
    9,
    8,
    15,
    14,
    13,
    12,
    19,
    18,
    17,
    16,
    23,
    22,
    21,
    20,
    27,
    26,
    25,
    24,
)
"""Dropping right to left top from bottom"""


DROP_PATTERN_SNAKE: typing.Final[typing.Tuple[int, ...]] = (
    0,
    1,
    2,
    3,
    7,
    6,
    5,
    4,
    8,
    9,
    10,
    11,
    15,
    14,
    13,
    12,
    16,
    17,
    18,
    19,
    23,
    22,
    21,
    20,
    24,
    25,
    26,
    27,
)
"""Dropping snake starting from right to left"""

DROP_PATTERN_SNAKE_R: typing.Final[typing.Tuple[int, ...]] = (
    3,
    2,
    1,
    0,
    4,
    5,
    6,
    7,
    11,
    10,
    9,
    8,
    12,
    13,
    14,
    15,
    19,
    18,
    17,
    16,
    20,
    21,
    22,
    23,
    27,
    26,
    25,
    24,
)
"""Dropping snake starting from left to right"""


DROP_PATTERN_TOPDOWN: typing.Final[typing.Tuple[int, ...]] = (
    0,
    4,
    8,
    12,
    16,
    20,
    24,
    1,
    5,
    9,
    13,
    17,
    21,
    25,
    2,
    6,
    10,
    14,
    18,
    22,
    26,
    3,
    7,
    11,
    15,
    19,
    23,
    27,
)
"""Dropping from top to bottom then right to left"""

DROP_PATTERN_TOPDOWN_R: typing.Final[typing.Tuple[int, ...]] = (
    3,
    7,
    11,
    15,
    19,
    23,
    27,
    2,
    6,
    10,
    14,
    18,
    22,
    26,
    1,
    5,
    9,
    13,
    17,
    21,
    25,
    0,
    4,
    8,
    12,
    16,
    20,
    24,
)
"""Dropping from top to bottom then left to right"""


DROP_PATTERN_SPIRAL: typing.Final[typing.Tuple[int, ...]] = (
    0,
    1,
    2,
    3,
    7,
    11,
    15,
    19,
    23,
    27,
    26,
    25,
    24,
    20,
    16,
    12,
    8,
    4,
    5,
    6,
    10,
    14,
    18,
    22,
    21,
    17,
    13,
    9,
)
"""Dropping in slot 0(top left corner of inv) and circles in a clockwise spiral"""

DROP_PATTERN_ALTERNATING: typing.Final[typing.Tuple[int, ...]] = (
    0,
    4,
    1,
    5,
    2,
    6,
    3,
    7,
    8,
    12,
    9,
    13,
    10,
    14,
    11,
    15,
    16,
    20,
    17,
    21,
    18,
    22,
    19,
    23,
    24,
    25,
    26,
    27,
)
"""Dropping left to right top to bottom 2 rows at a time"""


def scrape_inventory() -> None:
    """A simple util function which will scrape the inventory section of the screen"""
    scrape_rect(550, 200, 170, 260)


def inventory_slots() -> typing.Generator[SimpleNamespace, None, None]:
    for x in range(28):
        yield get_inventory_slot(x)


def is_slot_full(number: int) -> bool:
    """
    Returns a bool of the given inventory slot between 0 and 27

    :param number: int
        the inventory slot to check between 0-27
    :return: bool
        returns True if the slot has an item in it else False
    """
    erase_backbuffer()
    scrape_inventory()
    slot = get_inventory_slot(number)

    if find_color(
        0x000001, slot.x - 42 // 2, slot.y - 36 // 2, slot.x + 42 // 2, slot.y + 36 // 2
    ):
        return True
    return False


def get_empty_slots() -> typing.List[int]:
    """
    Returns the empty inventory slots based on the shadows of the items

    :return: typing.List[int]
        returns a list of the inventory slots that are empty
    """
    erase_backbuffer()
    scrape_inventory()
    empty_slots = list()

    for x, slot in enumerate(inventory_slots()):
        if not find_color(
            0x000001,
            slot.x - 42 // 2,
            slot.y - 36 // 2,
            slot.x + 42 // 2,
            slot.y + 36 // 2,
        ):
            empty_slots.append(x)

    return empty_slots


def get_inventory_slot(number: int) -> SimpleNamespace:
    """
    Returns the x and y coordinate of the given inventory slot between 0 and 27

    :param number: int
        the inventory slot to grab between 0-27
    :return: SimpleNamespace
        contains the x and y coordinate of the inventory slot
    """
    if not 0 <= number <= 27:  # make sure the number is between 0 and 27 inclusively
        raise IndexError(f"Inventory slot index out of range")

    # initializing variables
    x_start, y_start, delta_x, delta_y = 579, 229, 42, 36

    x = x_start + (number % 4 * delta_x)  # gets the x coordinate of the inventory slot
    y = y_start + (number // 4 * delta_y)  # gets the y coordinate of the inventory slot
    return SimpleNamespace(x=x, y=y)


def is_inventory_full() -> bool:
    """
    Checks if your inventory is full by checking your last inventory slot

    :return: bool
        returns True if the inventory is full, else returns False

    """
    return not bool(get_empty_slots())


def search_inventory(
    bitmaps: typing.Union[typing.Sequence[int], int], tolerance: int = 0
) -> typing.List[int]:
    """
    Searches the inventory for a bitmap
    Returns all instances of that bitmap

    :param bitmaps: typing.Union[typing.List[int], int]
        the bitmap(s) to search for
    :param tolerance: int
        The color tolerance to search with
    :return: typing.List[int]
         a list of inventory slots that were found to have the bitmap(s) in
    """
    found_slots = []
    if isinstance(bitmaps, int):
        bitmaps = [bitmaps]

    delta_x, delta_y = 42, 36
    scrape_inventory()

    for x, slot in enumerate(inventory_slots()):
        for bitmap in bitmaps:
            inv_square = (
                slot.x - delta_x // 2,
                slot.y - delta_y // 2,
                slot.x + delta_x // 2,
                slot.y + delta_y // 2,
            )
            if find_bitmap(bitmap, *inv_square, color_tolerance=tolerance):
                found_slots.append(x)
                break

    return found_slots


def drop_inventory_slots(
    *inv_nums: int, pattern: typing.Iterable[int] = DROP_PATTERN_ALTERNATING
) -> None:
    """
    Drops specific inventory slots indexed from 0

    :param inv_nums: int
        which inventory slots to drop
    :param pattern: typing.Iterable[int]
        the pattern to drop the items with. defaults to DROP_PATTERN_ALTERNATING
    :return: None
    """
    key_down(0x10)
    sleep(random.randint(5, 9))
    for inv_num in _drop_order(inv_nums, pattern):
        inv_location = get_inventory_slot(inv_num)
        click_random_circle_point(inv_location.x, inv_location.y, r=15)
    sleep(random.randint(5, 9))
    key_up(0x10)


def _drop_order(
    inv_nums: typing.Sequence[int], pattern: typing.Iterable[int]
) -> typing.Sequence[int]:
    return [slot for slot in pattern if slot in inv_nums]


def drop_items(
    bitmaps: typing.Union[typing.List[int], int],
    tolerance: int = 0,
    pattern: typing.Iterable[int] = DROP_PATTERN_ALTERNATING,
) -> None:
    """
    Drops items in your inventory

    :param bitmaps: typing.Union[typing.List[int], int]
        the bitmap(s) to search for
    :param tolerance: int
        the tolerance to search with
    :param pattern: typing.Iterable[int]
        the pattern to drop the items with. defaults to DROP_PATTERN_ALTERNATING
    :return: None
    """
    if isinstance(bitmaps, int):
        bitmaps = [bitmaps]

    drop_inventory_slots(*search_inventory(bitmaps, tolerance), pattern=pattern)
