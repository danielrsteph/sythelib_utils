#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["start", "login"]

from pylibsythe import *

from sythelib_utils import utils, Fonts


def start(title: str, testing: bool = False) -> None:
    """
    A startup method for RuneScape specifically.
    This will set the window to SunAwtCanvas and set the virtual mouse on.

    :param title: str
        The title of the window to click
    :param testing: bool
        A boolean if test mode should be on or off. If set on, it will take over your mouse.
    :return: None
    """
    set_window(title=title)
    while select_inner_window(class_name="SunAwtCanvas"):
        pass
    antigcp()

    if testing:
        set_virtual_mouse_java_mode_off()
        set_virtual_inputs_off()
    else:
        set_gdi_capture_on()
        set_virtual_mouse_java_mode_on()
    set_mouse_delay(0, 0)
    utils.move_mouse_to_edge(True)


def login(username: str, password: str, fonts: Fonts) -> bool:
    """
    Will log you into RuneScape. It will try to log you in up to the threshold before returning False.
    It will return True if it successfully logs in completely.
    It returns false if it either wasn't able to log you in, or you're already logged in.

    :param username: Your RuneScape username
    :param password: Your RuneScape password
    :param fonts: The fonts needed to login
    :return: bool
    """
    hit_enter = False

    scrape()
    # if the user was timed out
    if dc := find_text_using_font(fonts.font1, "Ok"):
        utils.click_random_square_point(**vars(dc))
        sleep(20)
        scrape()

    # looks for the existing user button
    if eu := find_text_using_font(fonts.font1, "Exist"):
        utils.click_random_square_point(**vars(eu))
        sleep(20)
        scrape()

    # looks for the login field and enters the username if so
    if log := find_text_using_font(fonts.font1, "Login:"):
        utils.click_random_square_point(**vars(log))
        _send_backspaces(25)
        send_keys(username)

    # looks for the password field and enters the password if so
    if pw := find_text_using_font(fonts.font1, "Password:"):
        utils.click_random_square_point(**vars(pw))
        _send_backspaces(25)
        send_keys(password)
        sleep(200)
        send_keys("\n")
        hit_enter = True

    # clicks the play button if found
    while hit_enter and not (play := find_text_using_font(fonts.font1, "HERETOPLAY")):
        scrape()
        pass
    else:
        if hit_enter:
            utils.click_random_square_point(**vars(play))

    return hit_enter


def _send_backspaces(n: int) -> None:
    for x in range(n):
        key_down(8)
        sleep(4)
        key_up(8)
        sleep(4)
